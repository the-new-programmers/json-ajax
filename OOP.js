function Car1() {
    this.lightsIsOn = false;

    this.turnOnLights = function() { // regular function
        this.lightsIsOn = true;
    }
}

let volvo = new Car1();

console.log(volvo.lightsIsOn); // false
volvo.turnOnLights();
console.log(volvo.lightsIsOn); // false


function Car2() {
    this.lightsIsOn = false;

    this.turnOnLights = () => { // arrow function
        this.lightsIsOn = true;
    }
}

let ford = new Car2();
console.log(ford.lightsIsOn); // false
ford.turnOnLights();
console.log(ford.lightsIsOn); // true