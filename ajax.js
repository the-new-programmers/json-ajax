// ajax 1
const xhttp = new XMLHttpRequest();
console.log('Event 1');

xhttp.onreadystatechange = () => {
    if (this.readyState == 4 && this.status == 200) {

        let data = JSON.parse(this.responseText);
        console.log('Event 3', data);

    }
};

xhttp.open("GET", "https://dev-api.com/memory-game/server-data/1", true);
xhttp.send();
console.log('Event 2');


// ajax 2
console.log('Event 1');
fetch('https://dev-api.com/memory-game/server-data/1')
    .then(response => {
        console.log('Event 3');
        return response.json();
    })
    .then(data => {
        console.log('Event 4');
        console.log(data);
    });

console.log('Event 2');