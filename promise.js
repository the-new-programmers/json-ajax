(()=>{})();


(() => {
    console.log('רצתי')
})();


new Promise((resolve) => {
    resolve('1234');
})
.then((data) => {
    console.log(data); // 1234
});


(async() => {

    let data = await fetch('https://dev-api.com/memory-game/server-data/1');

    console.log(data); // 1234

})();


(async() => {

    let data = (await new Promise((resolve) => {
        setTimeout(() => {
            resolve('1234');
        }, 3000);
    }));

    console.log(data); // 1234

})();



fetch('https://dev-api.com/memory-game/server-data/1')
    .then(response => response.json())
    .then(data => console.log(data) /* 1234 */);