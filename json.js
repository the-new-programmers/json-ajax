
let arr = ['David', 'Dana', 'Amnon'];

let setJson = JSON.stringify(arr); // =>  '["David","Dana","Amnon"]';


let obj = {
    name: 'David',
    func: function() {
        console.log('1234');
    },
    arr: [1,2,3,4,5],
    isActive: true
}

let cloneObj = JSON.parse(JSON.stringify(obj));
